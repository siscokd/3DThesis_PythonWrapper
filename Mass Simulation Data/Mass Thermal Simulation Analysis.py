# -*- coding: utf-8 -*-
"""
Created on Tue May 25 14:12:13 2021

@author: Kevin
"""


import glob
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


Simulations = glob.glob('*/')



fig, ax = plt.subplots()

for Simulation in Simulations[0:1]:
    Average = []; BeamVelocity = []
    
    Folders = glob.glob(Simulation + '*/')
    
    for Folder in Folders:    
        Data = pd.read_csv(glob.glob(Folder+'*.csv')[0])
        Data['V'] = np.abs(Data['V'])
        
        BeamVelocityCalc =  int(Folder.split('\\')[1].strip('AlCeMn'))
        AverageCalc = np.mean(Data['V'])
        Depth = np.min(Data['z'])
        Width = np.max(Data['y']) - np.min(Data['y'])
        

        Average.append(AverageCalc)
        BeamVelocity.append((BeamVelocityCalc))
        
    #plt.scatter(BeamVelocity,Average)

        ax.hist(Data['V'], label = BeamVelocityCalc, alpha = .5, bins = np.linspace(np.min(Data['V']),np.max(Data['V']),30))
    
    
ax.legend()


#