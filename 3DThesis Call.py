# -*- coding: utf-8 -*-
"""
Created on Thu Mar 18 14:21:02 2021

@author: Kevin
"""

import os
import subprocess
from GenerateCalls import ParameterGeneration
import numpy as np
import glob
import pandas as pd
import matplotlib.pyplot as plt
Executable_Location = os.getcwd()+ '\\3DThesis.exe'

Path = os.getcwd() + '\Path.txt'




Velocities = np.logspace(-2,0,10)*1000 # mm/s

for V in Velocities:
    
    V = round(V,-1)
    
    Number = str(int(V)).zfill(5)
    
    PG = ParameterGeneration(name = 'AlCeMn' + Number, pathfile = Path)
    
    PG.WritePathFile(V/1000)    
    PG.UpdateMaterialParameters(Preheat_Temperature = 200+273 ,Liquidus_Temperature = 723, ThermalConductivity =190, SpecificHeat =1080,Density = 2500)
    PG.UpdateBeamParameters(Power = 300, Efficiency = .35, Width_X = 100e-6, Width_Y = 100e-6, Depth_Z = 10e-6)
    PG.UpdateSettings(Timestep= 1e-4, Mode = 2, MaxThreads = 8,Output_Mode = 1)
    PG.UpdateDomain(X_Values = [.0025,.0025,1],Y_Values = [-1e-3,.002,1e-6],Z_Values = [-.001,0,1e-6])
    Arguments = PG.Write()
    args = Executable_Location
    subprocess.call(args)


